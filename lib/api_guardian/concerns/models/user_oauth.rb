# frozen_string_literal: true

module ApiGuardian
  module Concerns
    module Models
      module User 
        module UserOauth
          extend ActiveSupport::Concern
          included do
            has_many :oauth_applications,
                    class_name: 'Doorkeeper::Application',
                    as: :owner

            has_many :access_grants, class_name: "Doorkeeper::AccessGrant",
                    foreign_key: :resource_owner_id,
                    dependent: :delete_all # or :destroy if you need callbacks

            has_many :access_tokens, class_name: "Doorkeeper::AccessToken",
                    foreign_key: :resource_owner_id,
                    dependent: :delete_all # or :destroy if you need callbacks

            before_create :create_application
          end

          private
            def create_application
              app = oauth_applications.new name: "#{self.email} API",
                                          redirect_uri: 'https://oauth.eggpay.kr'
              app.save!
            end
        end
      end
    end
  end
end


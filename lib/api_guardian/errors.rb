module ApiGuardian
  module Errors
    class Error < StandardError
    end

    class IdentityAuthorizationFailed < Error; end
    class InvalidContentType < Error; end
    class InvalidJwtSecret < Error; end
    class InvalidPermissionName < Error; end
    class InvalidAuthenticationProvider < Error; end
    class InvalidRegistrationProvider < Error; end
    class InvalidRequestBody < Error; end
    class InvalidRequestResourceId < Error; end
    class InvalidRequestResourceType < Error; end
    class InvalidUpdateAction < Error; end
    class PasswordInvalid < Error; end
    class PasswordRequired < Error; end
    class PhoneNumberInvalid < Error; end
    class RegistrationValidationFailed < Error; end
    class ResetTokenExpired < Error; end
    class ResetTokenUserMismatch < Error; end
    class TwoFactorRequired < Error; end
    class UserInactive < Error; end
    class ResourceStoreMissing < Error; end
    class ResourceClassMissing < Error; end
    class GuestAuthenticationDisabled < Error; end
    class OperationClassMissing < Error; end
    class PolicyClassMissing < Error; end
    class PolicyError < Error; end
    class SerializerClassMissing < Error; end
    class ResponseModelClassMissing < Error; end    
    class OperationError < Error
      attr_reader :code
      def initialize(message, code)
        super(message)
        @code = code
      end
    end

    class ApiError < Error
      attr_reader :code, :status
      def initialize(message, code, status)
        super(message)
        @code = code
        @status = status
      end
    end

    class RecordInvalid < Error

      attr_reader :record

      def initialize(record = nil)
        if record
          @record = record
          errors = @record.errors
          message ="Record invalid"# I18n.t(:"#{@record.class.i18n_scope}.errors.messages.record_invalid", errors: errors, default: :"errors.messages.record_invalid")
        else
          message = "Record invalid"
        end
  
        super(message)
      end
    end    
  end
end

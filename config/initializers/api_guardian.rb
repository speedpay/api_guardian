# ActiveModel::Serializer.config.adapter = nil

api_mime_types = %w(
  application/vnd.api+json
  text/x-json
  application/json
)

Mime::Type.unregister :json
Mime::Type.register 'application/json', :json, api_mime_types

# Raven.configure do |config|
#   config.dsn = ENV['RAVEN_URL']
# end